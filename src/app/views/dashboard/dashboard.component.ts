import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Chart } from 'chart.js'
import { CommonModule } from "@angular/common";
@Component({
  templateUrl: 'dashboard.component.html',

  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  Totalmembers = [];
  animalname = [];
  total = [];
  chartnamw = [];
  chartnamw1 = [];
  Salebystate = [];
  statename = [];
  statetotal = [];
  color = [];
  colors: string = 'color';
  moredetails: boolean = true;
  // dashboarddata = [];
  dashboarddatacount = [];
  dashboarddate = [];
  totalcountbystate = 0;
  totalcountbyanimal = 0;
  districttotal = [];
  salebydistrict = [];
  districtname = [];
  totaldistrictbycount = 0;
  dropdowndata = [{name:"Andhra Pradesh"},{name:"karnataka"},{name:"Maharashtra"},{name:"Telangana"}];
  department = "Andhra Pradesh";
  dropdownoption = [{name:"By Year",value:"year"},{name:"By Month",value:"month"},{name:"By Week",value:"week"}];
  dropdownselected = "year";
  dropdownselectedcanvas = "year";
  constructor(private apiService: ApiService, private route: Router, private AmCharts: AmChartsService, ) {
  }

  ngOnInit(): void {
    this.apiService.getdashboardbyanimal('year')
      .subscribe(
        (data: any) => {
          console.log(data);
           this.color = ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
           this.Totalmembers = data.data.sale_by_animal;
           this.totalcountbyanimal = data.data.total_sale_by_animal;
           this.Totalmembers.forEach((index, data1) => {
             this.total.push(index.count);
            this.animalname.push(index.species);
           })
          
           this.Totalmembers.forEach((index2, data4) => {
             this.Totalmembers[data4][this.colors] = this.color[data4];

           });
          this.chartnamw = new Chart('canvas', {
            type: 'doughnut',

            data: {
              datasets: [{
                label: 'Colors',
                data: this.total,
                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
              }],
              labels: this.animalname
            },
            options: {
              responsive: true, cutoutPercentage: 75,
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              centertext: this.totalcountbyanimal
            }, plugins: [{
              beforeDraw: function (chart) {
                if (chart.options.centertext) {
                  var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;

                  ctx.restore();
                  var fontSize = (height / 80).toFixed(2); // was: 114
                  ctx.font = fontSize + "em sans-serif";
                  ctx.textBaseline = "middle";

                  var text = chart.options.centertext, // "75%",
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = Math.round(height/1.6) ;
                  ctx.fillText(text, textX, textY);
                  ctx.save();
                }
              }
            }],
          });

        }, error => {

        });



        this.apiService.getdashboardbystate('year').subscribe((data:any)=>{
           this.Salebystate = data.data.sale_by_state.slice(0, 4);
           this.totalcountbystate = data.data.total_sale_by_state;
           
          this.Salebystate.forEach((index1, data2) => {
            
            this.statename.push(index1.state);
            this.statetotal.push(index1.count);
          });
           
          this.Salebystate.forEach((index3, data5) => {
            this.Salebystate[data5][this.colors] = this.color[data5]
          });
          this.chartnamw = new Chart('canvas1', {
            type: 'doughnut',
            data: {
              datasets: [{
                label: 'Colors',
                data: this.statetotal,
                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
              }],
              labels: this.statename
            },
            options: {
              responsive: true,
              cutoutPercentage: 75,
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              tooltips: {
                enabled: true
              },
              centertext: this.totalcountbystate
            }, plugins: [{
              beforeDraw: function (chart) {
                if (chart.options.centertext) {
                  var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;

                  ctx.restore();
                  var fontSize = (height / 80).toFixed(2); // was: 114
                  ctx.font = fontSize + "em sans-serif";
                  ctx.textBaseline = "middle";

                  var text = chart.options.centertext, // "75%",
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = Math.round(height/1.6) ;
                  ctx.fillText(text, textX, textY);
                  ctx.save();
                }
              }
            }],
          });
        },error=>{

        });


        this.apiService.getdashboardbydistrict('Andhra Pradesh','year').subscribe((data:any)=>{
          console.log(data);
          this.salebydistrict = data.data.sale_by_state.slice(0, 4);
          this.totaldistrictbycount = data.data.total_sale_by_state;
          
         this.salebydistrict.forEach((index1, data2) => {
           
           this.districtname.push(index1.district);
           this.districttotal.push(index1.count);
         });
          
         this.salebydistrict.forEach((index3, data5) => {
           this.salebydistrict[data5][this.colors] = this.color[data5]
         });
         console.log(this.salebydistrict,"======",this.totaldistrictbycount,"====",this.districtname,"===",this.districttotal);
         this.chartnamw = new Chart('canvas3', {
           type: 'doughnut',
           data: {
             datasets: [{
               label: 'Colors',
               data: this.districttotal,
               backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
             }],
             labels: this.districtname
           },
           options: {
             responsive: true,
             cutoutPercentage: 75,
             title: {
               display: true,
               text: ""
             },
             legend: {
               display: false
             },
             tooltips: {
               enabled: true
             },
             centertext: this.totaldistrictbycount
           }, plugins: [{
             beforeDraw: function (chart) {
               if (chart.options.centertext) {
                 var width = chart.chart.width,
                   height = chart.chart.height,
                   ctx = chart.chart.ctx;

                 ctx.restore();
                 var fontSize = (height / 80).toFixed(2); // was: 114
                 ctx.font = fontSize + "em sans-serif";
                 ctx.textBaseline = "middle";

                 var text = chart.options.centertext, // "75%",
                   textX = Math.round((width - ctx.measureText(text).width) / 2),
                   textY = Math.round(height/1.6) ;
                 ctx.fillText(text, textX, textY);
                 ctx.save();
               }
             }
           }],
         });
        },error=>{});


    let id = 3;
    this.apiService.getdashboardbydate(id)
      .subscribe(
        (data: any) => {
          data.forEach(element => {
            this.dashboarddate.push(element[0].current_date);
            this.dashboarddatacount.push(element[0].count);

          });
          this.chartnamw = new Chart('canvas2', {
            type: 'line',
            data: {
              datasets: [{
                label: 'Colors',
                data: this.dashboarddatacount,
                //backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
                // label: 'Dique Las Maderas',
                fill: false,
                borderColor: "#00BFFF",


              }],
              labels: this.dashboarddate
            },
            options: {
              responsive: true,
              elements: {
                line: {
                  tension: 0,
                  color: 'blue'
                }
              },
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              tooltips: {
                enabled: false
              },
              plugins: {
                p1: false
              }
            }
          });
          
        }, error => {

        });


  }
  onClickMe() {
    this.moredetails = false;
    this.apiService.getdashboardbyanimal('year')
      .subscribe(
        (data: any) => {
          this.statename = [];
          this.statetotal = [];
          this.Salebystate = data.data.sale_by_state;
          this.Salebystate.forEach((index1, data2) => {
            this.statename.push(index1.state);
            this.statetotal.push(index1.count);
          })
          this.Salebystate.forEach((index3, data5) => {
            this.Salebystate[data5][this.colors] = this.color[data5]
          })
          this.chartnamw = new Chart('canvas1', {

            type: 'doughnut',
            data: {
              datasets: [{
                label: 'Colors',
                data: this.statetotal,
                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
              }],
              labels: this.statename
            },
            options: {
              responsive: true, cutoutPercentage: 75,
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              tooltips: {
                enabled: true
              },
              centertext: this.totalcountbystate
            }, plugins: [{
              beforeDraw: function (chart) {
                if (chart.options.centertext) {
                  var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;

                  ctx.restore();
                  var fontSize = (height / 80).toFixed(2);
                  ctx.font = fontSize + "em sans-serif";
                  ctx.textBaseline = "middle";

                  var text = chart.options.centertext,
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = height / 2;
                  ctx.fillText(text, textX, 100);
                  ctx.save();
                }
              }
            }],
          });

        }, error => {

        }
      )

  }
  clickme(event) {
    this.route.navigate(['/animalregistration/animals/', event]);
  }
  linebyweek(id) {
    this.dashboarddatacount = [];
    this.dashboarddate = [];
    this.apiService.getdashboardbydate(id)
      .subscribe(
        (data: any) => {
          data.forEach(element => {
            this.dashboarddate.push(element[0].current_date);
            this.dashboarddatacount.push(element[0].count);

          });
          this.chartnamw = new Chart('canvas2', {
            type: 'line',
            data: {
              datasets: [{
                label: 'Colors',
                data: this.dashboarddatacount,
                //backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
                // label: 'Dique Las Maderas',
                fill: false,
                borderColor: "#00BFFF",


              }],
              labels: this.dashboarddate
            },
            options: {
              responsive: true,
              elements: {
                line: {
                  tension: 0,
                  color: 'blue'
                }
              },
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              tooltips: {
                enabled: false
              },
              plugins: {
                p1: false
              }
            }
          });
        }, error => {

        });

  }
  breedbydata(id){
    this.apiService.getdashboardbyanimal(id.target.value)
    .subscribe((data:any)=>{
      this.Totalmembers = [];
      this.animalname = [];
      this.total = [];
      this.color = ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
           this.Totalmembers = data.data.sale_by_animal;
           this.totalcountbyanimal = data.data.total_sale_by_animal;
           this.Totalmembers.forEach((index, data1) => {
             this.total.push(index.count);
            this.animalname.push(index.species);
           })
           this.Totalmembers.forEach((index2, data4) => {
             this.Totalmembers[data4][this.colors] = this.color[data4];
           });
           this.chartnamw = new Chart('canvas', {
            type: 'doughnut',

            data: {
              datasets: [{
                label: 'Colors',
                data: this.total,
                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
              }],
              labels: this.animalname
            },
            options: {
              responsive: true, cutoutPercentage: 75,
              title: {
                display: true,
                text: ""
              },
              legend: {
                display: false
              },
              centertext: this.totalcountbyanimal
            }, plugins: [{
              beforeDraw: function (chart) {
                if (chart.options.centertext) {
                  var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;

                  ctx.restore();
                  var fontSize = (height / 80).toFixed(2); // was: 114
                  ctx.font = fontSize + "em sans-serif";
                  ctx.textBaseline = "middle";

                  var text = chart.options.centertext, // "75%",
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = Math.round(height/1.6) ;
                  ctx.fillText(text, textX, textY);
                  ctx.save();
                }
              }
            }],
          });

    })
  }

  statebydata(id){
    console.log(id.target.value);
    this.statename = [];
    this.statetotal = [];
    this.apiService.getdashboardbystate(id.target.value).subscribe((data:any)=>{
      this.Salebystate = data.data.sale_by_state.slice(0, 4);
      this.totalcountbystate = data.data.total_sale_by_state;
      
     this.Salebystate.forEach((index1, data2) => {
       
       this.statename.push(index1.state);
       this.statetotal.push(index1.count);
     });
      
     this.Salebystate.forEach((index3, data5) => {
       this.Salebystate[data5][this.colors] = this.color[data5]
     });
     this.chartnamw = new Chart('canvas1', {
       type: 'doughnut',
       data: {
         datasets: [{
           label: 'Colors',
           data: this.statetotal,
           backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
         }],
         labels: this.statename
       },
       options: {
         responsive: true,
         cutoutPercentage: 75,
         title: {
           display: true,
           text: ""
         },
         legend: {
           display: false
         },
         tooltips: {
           enabled: true
         },
         centertext: this.totalcountbystate
       }, plugins: [{
         beforeDraw: function (chart) {
           if (chart.options.centertext) {
             var width = chart.chart.width,
               height = chart.chart.height,
               ctx = chart.chart.ctx;

             ctx.restore();
             var fontSize = (height / 80).toFixed(2); // was: 114
             ctx.font = fontSize + "em sans-serif";
             ctx.textBaseline = "middle";

             var text = chart.options.centertext, // "75%",
               textX = Math.round((width - ctx.measureText(text).width) / 2),
               textY = Math.round(height/1.6) ;
             ctx.fillText(text, textX, textY);
             ctx.save();
           }
         }
       }],
     });
   },error=>{

   });
  }


  datebydistrict(id){
    this.salebydistrict = [];
    this.districtname = [];
    this.districttotal = [];
    this.apiService.getdashboardbydistrict(id.target.value,'year').subscribe((data:any)=>{
      this.salebydistrict = data.data.sale_by_state.slice(0, 4);
      this.totaldistrictbycount = data.data.total_sale_by_state;
      
     this.salebydistrict.forEach((index1, data2) => {
       
       this.districtname.push(index1.district);
       this.districttotal.push(index1.count);
     });
      
     this.salebydistrict.forEach((index3, data5) => {
       this.salebydistrict[data5][this.colors] = this.color[data5]
     });
     //console.log(this.salebydistrict,"======",this.totaldistrictbycount,"====",this.districtname,"===",this.districttotal);
     this.chartnamw = new Chart('canvas3', {
       type: 'doughnut',
       data: {
         datasets: [{
           label: 'Colors',
           data: this.districttotal,
           backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
         }],
         labels: this.districtname
       },
       options: {
         responsive: true,
         cutoutPercentage: 75,
         title: {
           display: true,
           text: ""
         },
         legend: {
           display: false
         },
         tooltips: {
           enabled: true
         },
         centertext: this.totaldistrictbycount
       }, plugins: [{
         beforeDraw: function (chart) {
           if (chart.options.centertext) {
             var width = chart.chart.width,
               height = chart.chart.height,
               ctx = chart.chart.ctx;

             ctx.restore();
             var fontSize = (height / 80).toFixed(2); // was: 114
             ctx.font = fontSize + "em sans-serif";
             ctx.textBaseline = "middle";

             var text = chart.options.centertext, // "75%",
               textX = Math.round((width - ctx.measureText(text).width) / 2),
               textY = Math.round(height/1.6) ;
             ctx.fillText(text, textX, textY);
             ctx.save();
           }
         }
       }],
     });
    },error=>{});

  }
  // animalbyweek(id){
  //   this.apiService.getdashboardbyanimal('year')
  //     .subscribe(
  //       (data: any) => {
  //         this.statename = [];
  //         this.statetotal = [];
  //         this.Salebystate = data.data.sale_by_state;
  //         this.Salebystate.forEach((index1, data2) => {
  //           this.statename.push(index1.state);
  //           this.statetotal.push(index1.Count);
  //         })
  //         this.Salebystate.forEach((index3, data5) => {
  //           this.Salebystate[data5][this.colors] = this.color[data5]
  //         })
  //         this.chartnamw = new Chart('canvas1', {

  //           type: 'doughnut',
  //           data: {
  //             datasets: [{
  //               label: 'Colors',
  //               data: this.statetotal,
  //               backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
  //             }],
  //             labels: this.statename
  //           },
  //           options: {
  //             responsive: true, cutoutPercentage: 75,
  //             title: {
  //               display: true,
  //               text: ""
  //             },
  //             legend: {
  //               display: false
  //             },
  //             tooltips: {
  //               enabled: true
  //             },
  //             centertext: this.totalcountbystate
  //           }, plugins: [{
  //             beforeDraw: function (chart) {
  //               if (chart.options.centertext) {
  //                 var width = chart.chart.width,
  //                   height = chart.chart.height,
  //                   ctx = chart.chart.ctx;

  //                 ctx.restore();
  //                 var fontSize = (height / 80).toFixed(2);
  //                 ctx.font = fontSize + "em sans-serif";
  //                 ctx.textBaseline = "middle";

  //                 var text = chart.options.centertext,
  //                   textX = Math.round((width - ctx.measureText(text).width) / 2),
  //                   textY = height / 2;
  //                 ctx.fillText(text, textX, 100);
  //                 ctx.save();
  //               }
  //             }
  //           }],
  //         });

  //       }, error => {

  //       }
  //     )

  // }
}
