import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.scss']
  })
  export class ViewComponent implements OnInit {
    Totalreport = [];
    Totalreporttemp = [];
    cols = [
      { header: "Username", field: "user_name" },
      { header: "Animal ID", field: "animal_id" },
      {header:"Report",field:"report"}
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      this.apiService.viewrequest()
      .subscribe(
        (data: any) => {
          this.Totalreport = data.data;
          this.Totalreporttemp = data.data;
          console.log(data);
        }, error => {
         
        }); 
   
  }
//   search(event) {
//     let value = event.target.value.toLowerCase();
//     console.log(value.length);
//     let temp = [];
//     for (let member of this.Totalmembers) {
//       for (let key in member) {
//         if (member[key].toString().toLowerCase().includes(value)) {
//           temp.push(member);
//           break;
//         }
//       }
//     }
//     this.Totalmembers = temp;
//     if (value.length === 0) {
//       this.Totalmembers = this.Totalmemberstemp;
//     }
//   }
}