import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';

import { TicketingRoutingModule } from './ticketing-routing.module';
import {TicketingComponent} from './ticketing.component'
import {ViewComponent} from './view/view.component';

@NgModule({
  declarations: [TicketingComponent,
    ViewComponent],
  imports: [
    CommonModule,
    TicketingRoutingModule,
    FormsModule,
    TableModule
  ]
})
export class TicketingModule { }
