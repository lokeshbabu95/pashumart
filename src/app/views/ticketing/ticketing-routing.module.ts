import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TicketingComponent} from './ticketing.component'
import {ViewComponent} from './view/view.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Request'

    },
    children: [
       {
        path: 'add',
        component: TicketingComponent,
        data: {
          title: 'ADD complaint'
        }
      },
      {
        path:'view',
        component:ViewComponent,
        data: {
          title: 'Review complaint'
        }
      }
      
      
     
   ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TicketingRoutingModule { }
