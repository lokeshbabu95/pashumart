import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-food',
    templateUrl: './foodview.component.html',
    styleUrls: ['./foodview.component.scss']
  })
  export class FoodviewComponent implements OnInit {
    logisticsloginid;
    logisticsdata;
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
        console.log(this.route.url);
        console.log(this.route.url.split("/"));
        this.logisticsloginid = this.route.url.split("/");
        this.apiService.viewfoodbyid(this.logisticsloginid[3],this.logisticsloginid[4])
      .subscribe(
        (data: any) => {
          this.logisticsdata = data.data[0];
          console.log(this.logisticsdata);
        }, error => {
         
        });
   
  }

}