import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-food',
    templateUrl: './food.component.html',
    styleUrls: ['./food.component.scss']
  })
  export class FoodComponent implements OnInit {
    Totalmembers = [];
    Totalmemberstemp =[];
    cols = [
      { header: "User Name", field: "mobile_number" },
      { header: "Name", field: "name" },
      { header: "Mobile", field: "mobile_number" },
      {header:"Type of Food",field:"type_of_food"},
      {header:"Quanitity",field:"quantity_avaliable"},
      {header:"Price",field:"price"},
      { header: "State", field: "state" },
      { header: "District", field: "district" },
      { header: "Location", field: "location" },
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      this.apiService.viewfood()
      .subscribe(
        (data: any) => {
          this.Totalmembers = data.data;
          this.Totalmemberstemp = data.data;
          console.log(data);
        }, error => {
         
        });
   
  }
  search(event) {
    let value = event.target.value.toLowerCase();
    console.log(value.length);
    let temp = [];
    for (let member of this.Totalmembers) {
      for (let key in member) {
        if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }
      }
    }
    this.Totalmembers = temp;
    if (value.length === 0) {
      this.Totalmembers = this.Totalmemberstemp;
    }
  }

  viewdata(details){
    console.log(details);
    let urk = "/partner/food/"+details.login_id+"/"+details.foodid;
    console.log(urk);
    console.log('/partner/food/',details.login_id,'/',details.foodid)
    this.route.navigate(['/partner/food/'+details.login_id+'/'+details.foodid]);
  }
}