import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-food',
    templateUrl: './logisticsview.component.html',
    styleUrls: ['./logisticsview.component.scss']
  })
  export class LogisticsviewComponent implements OnInit {
    logisticsloginid;
    logisticsdata;
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
        console.log(this.route.url);
        console.log(this.route.url.split("/"));
        this.logisticsloginid = this.route.url.split("/");
        this.apiService.viewlogisticsbyId(this.logisticsloginid[3])
      .subscribe(
        (data: any) => {
          this.logisticsdata = data.data[0];
          console.log(this.logisticsdata);
        }, error => {
         
        });
   
  }

}