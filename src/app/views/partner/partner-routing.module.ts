import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PartnerComponent} from './partner.component';
import { LogisticsComponent } from './logistics/logistics.component';
import {FoodComponent} from './food/food.component';
import {LoanComponent} from './loan/loan.component';
import {DoctorviewComponent} from './doctorview/doctorview.component';
import {LogisticsviewComponent} from './logisticsview/logisticsview.component';
import {LoanviewComponent} from './loanview/loanview.component';
import {FoodviewComponent} from './foodview/foodview.component';


const routes: Routes = [
    {
      path: '',
      data: {
        title: 'partners'

      },
      children: [
         {
          path: 'doctors',
          component: PartnerComponent,
          data: {
            title: 'doctor'
          }
        },
        {
          path:'logistics',
          component:LogisticsComponent,
          data:{
            title:'Logistics'
          }
        },
        {
          path:'food',
          component:FoodComponent,
          data:{
            title:'Food'
          }
        },
        {
          path:'doctor/:id',
          component:DoctorviewComponent,
          data:{
            title:'view doctor'
          }
        },
        {
          path:'logistics/:id',
          component:LogisticsviewComponent,
          data:{
            title:'view logistics'
          }
        },
        {
          path:'loan',
          component:LoanComponent,
          data:{
            tittle:'Loan'
          }
        },
        {
          path:'loan/:id/:id',
          component:LoanviewComponent,
         
            data:{
                  tittle:'Loan detail'
           
          }
        },
        {
          path:'food/:id/:id',
          component:FoodviewComponent,
          data:{

            tittle:'Food detail'
          }
        }
        
       
     ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class PartnerRoutingModule {}
  