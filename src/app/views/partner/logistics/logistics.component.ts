import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-logitsics',
    templateUrl: './logistics.component.html',
    styleUrls: ['./logistics.component.scss']
  })
  export class LogisticsComponent implements OnInit {
    Totalmembers = [];
    Totalmemberstemp =[];
    viewdetails:boolean = true;
    details;
    cols = [
      { header: "User Name", field: "mobile_number" },
      { header: "Name", field: "name" },
      { header: "Mobile", field: "mobile_number" },
      { header: "State", field: "state" },
      { header: "District", field: "district" },
      { header: "Location", field: "location" },
      { header: "Avaliability", field: "avaliability" },
      {header:"Vehicle",field:"vehicle_type"}
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      this.apiService.viewLogistics()
      .subscribe(
        (data: any) => {
          this.Totalmembers = data.data;
          this.Totalmemberstemp = data.data;
          console.log(data);
        }, error => {
         
        });
   
  }
  search(event) {
    let value = event.target.value.toLowerCase();
    console.log(value.length);
    let temp = [];
    for (let member of this.Totalmembers) {
      for (let key in member) {
        if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }
      }
    }
    this.Totalmembers = temp;
    if (value.length === 0) {
      console.log("iamhere");
      this.Totalmembers = this.Totalmemberstemp;
    }
  }
  viewdata(details){
    this.viewdetails = false;
    this.details = details;
    // console.log(details);
    this.route.navigate(['/partner/logistics/',details.login_id]);

  }
}