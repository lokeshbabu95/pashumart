import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-food',
    templateUrl: './doctorview.component.html',
    styleUrls: ['./doctorview.component.scss']
  })
  export class DoctorviewComponent implements OnInit {
    doctorloginid;
    doctordata;
    treatment = [];
    cols = [
        { header: "AnimalId", field: "animal_id" },
        { header: "Species", field: "species" },
        { header: "Disease", field: "disease" },
        { header: "Vaccine", field: "vaccine" },
        { header: "Dose", field: "doses" },
        { header: "Treated On", field: "treated_on" }
        //{header:""}
      ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
        console.log(this.route.url);
        console.log(this.route.url.split("/"));
        this.doctorloginid = this.route.url.split("/");
        this.apiService.viewdoctorbyId(this.doctorloginid[3])
      .subscribe(
        (data: any) => {
          this.doctordata = data.doctor_data[0];
          this.treatment = data.treatments;
          console.log(this.doctordata);
          console.log(this.treatment);
        }, error => {
         
        });
   
  }

}