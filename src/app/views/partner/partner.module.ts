import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {PartnerComponent} from './partner.component';
import {PartnerRoutingModule} from './partner-routing.module';
import { TableModule } from 'primeng/table';
import {LogisticsComponent} from './logistics/logistics.component';
import {FoodComponent} from './food/food.component';
import {LoanComponent} from './loan/loan.component'
import {DoctorviewComponent} from './doctorview/doctorview.component';
import {LogisticsviewComponent} from './logisticsview/logisticsview.component';
import {LoanviewComponent} from './loanview/loanview.component';
import {FoodviewComponent} from './foodview/foodview.component';
@NgModule({
    imports: [
      CommonModule,
      PartnerRoutingModule,
      TableModule
    ],
    declarations: [
        PartnerComponent,
        LogisticsComponent,
        FoodComponent,
        LoanComponent,
        DoctorviewComponent,
        LogisticsviewComponent,
        LoanviewComponent,
        FoodviewComponent
    ]
  })
  export class PartnerModule { }