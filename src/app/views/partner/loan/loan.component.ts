import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
@Component({
    selector: 'app-food',
    templateUrl: './loan.component.html',
    styleUrls: ['./loan.component.scss']
  })
  export class LoanComponent implements OnInit {
    Totalmembers = [];
    Totalmemberstemp =[];
    cols = [
      { header: "User Name", field: "mobile_number" },
      {header:"Name",field:"name"},
      {header:"Loan amount",field:"requested_loan_amount"},
      {header:"Bank",field:"bank_name"},
      {header:"Status",field:"status"}
     
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      this.apiService.viewLoan()
      .subscribe(
        (data: any) => {
          this.Totalmembers = data.data;
          this.Totalmemberstemp = data.data;
          console.log(data);
        }, error => {
         
        });
   
  }
  search(event) {
    let value = event.target.value.toLowerCase();
    console.log(value.length);
    let temp = [];
    for (let member of this.Totalmembers) {
      for (let key in member) {
        if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }
      }
    }
    this.Totalmembers = temp;
    if (value.length === 0) {
      this.Totalmembers = this.Totalmemberstemp;
    }
  }
  viewdata(details){
   
     console.log(details);
    this.route.navigate(['/partner/loan/'+details.login_id+'/'+details.id]);

  }
}