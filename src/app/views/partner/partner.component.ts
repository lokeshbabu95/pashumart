import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
@Component({
    selector: 'app-partner',
    templateUrl: './partner.component.html',
    styleUrls: ['./partner.component.scss']
  })
  export class PartnerComponent implements OnInit {
    Totalmembers = [];
    Totalmemberstemp =[];
    //viewdetails:boolean = true;
    details;
    cols = [
      {header:"Registration Id",field:"id"},
      { header: "User Name", field: "mobile_number" },
      { header: "Name", field: "name" },
      { header: "Mobile", field: "mobile_number" },
      { header: "State", field: "state" },
      { header: "District", field: "district" },
      { header: "Location", field: "location" },
      { header: "Avaliability", field: "avaliability" },
      {header:"Treatment",field:"treatment"}
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      console.log(this.route.url);
      this.apiService.viewDoctors()
      .subscribe(
        (data: any) => {
          this.Totalmembers = data.data;
          this.Totalmemberstemp = data.data;
        }, error => {
         
        });
   
  }
  search(event) {
    let value = event.target.value.toLowerCase();
    let temp = [];
    for (let member of this.Totalmembers) {
      for (let key in member) {
        if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }
      }
    }
    this.Totalmembers = temp;
    if (value.length === 0) {
      this.Totalmembers = this.Totalmemberstemp;
    }
  }
  viewdata(details){
    
     this.route.navigate(['/partner/doctor/',details.login_id]);

  }
}