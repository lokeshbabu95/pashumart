import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  Totalanimals = [];
  Totalanimalstemp = [];
  memberid = [];
  cols = [
    { header: "User Name", field: "mobile_number" },
    { header: "Name", field: "name" },
    { header: "Mobile", field: "mobile_number" },
    { header: "User Type", field: "user_type" },
     {header:"Total Animals",field:"count"},
    {header:"Sold", field:"sale"}
  ];
  urldata = [];
  constructor(private apiService: ApiService, private route: Router) {

    //console.log(this.urldata.split("/"));

  }
  ngOnInit() {
    this.memberid = this.route.url.split("/");
    this.apiService.employeemember( this.memberid[2])
      .subscribe(
        (data: any) => {
          this.Totalanimals = data.data;
          this.Totalanimalstemp = data.data;
          this.urldata = this.route.url.split("/");
          // if (this.urldata[3]) {
          //   this.search(this.urldata[3]);
          // }
        }, error => {

        });


  }
  onClickMe(animaldata) {
    console.log(animaldata);
    this.route.navigate(['/registeredmembers/', animaldata.login_id]);
  }
  search(event) {
    let value;
    if (event.target) {
      value = event.target.value.toLowerCase();
    } else {
      value = event.toLowerCase();
    }
    //let value = event.target.value.toLowerCase();
    let temp = [];
    for (let member of this.Totalanimals) {
      for (let key in member) {
        if (member[key] == null) {
        }
        else {
          if (member[key].toString().toLowerCase().includes(value)) {
            temp.push(member);
            break;
          }
        }

      }
    }
    this.Totalanimals = temp;
    if (value.length === 0) {
      console.log("iamhere");
      this.Totalanimals = this.Totalanimalstemp;
    }
  }
}