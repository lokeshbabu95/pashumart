import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {EmployeeComponent} from './employee.component';
import {EmployeeRoutingModule} from './employee-routing.module';
import{ViewComponent} from './view/view.component';


import { TableModule } from 'primeng/table';
@NgModule({
    imports: [
      CommonModule,
      EmployeeRoutingModule,
      TableModule
      
    ],
    declarations: [
        EmployeeComponent,
        ViewComponent
    ]
  })
  export class EmployeeModule { }