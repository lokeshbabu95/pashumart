import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../api.service';

@Component({
    selector: 'app-animal',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.scss']
  })
  export class EmployeeComponent implements OnInit {
    Totalanimals = [];
    Totalanimalstemp = [];
        cols = [
      
      { header: "User ID", field: "mobile_number" },
      { header: "Name", field: "name" },
      { header: "state", field: "state" },
      { header: "District", field: "district" },
      {header:"Members",field:"total_member"},
      {header:"Total Animals",field:"total_animal"},
      {header:"Sold", field:"total_sold"}
    ];
    urldata = [];
    constructor(private apiService: ApiService, private route: Router) {
    
      //console.log(this.urldata.split("/"));

    }
    ngOnInit() {
      this.apiService.viewemployee()
      .subscribe(
        (data: any) => {
          this.Totalanimals = data.data;
          this.Totalanimalstemp =data.data;
          this.urldata =  this.route.url.split("/");
          if(this.urldata[3]){
            this.search(this.urldata[3]);
          }
        }, error => {
         
        });
       
   
  }
  onClickMe(animaldata){
    console.log(animaldata);
    // this.route.navigate(['/animalregistration/animal/',animaldata.animal_id]);
  }
  search(event) {
    let value ;
    if(event.target){
     value = event.target.value.toLowerCase();
    }else{
      value = event.toLowerCase();
    }
    //let value = event.target.value.toLowerCase();
    let temp = [];
    for (let member of this.Totalanimals) {
      for (let key in member) {
        if(member[key] == null){
        }
        else{
          if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }}
        
      }
    }
    this.Totalanimals = temp;
    if (value.length === 0) {
      console.log("iamhere");
      this.Totalanimals = this.Totalanimalstemp;
    }
  }
  viewdata(rowData){
    console.log(rowData);
    this.route.navigate(['/employee/',rowData.login_id]);
  }
}