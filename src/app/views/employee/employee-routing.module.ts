import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeComponent} from './employee.component';
import {ViewComponent} from './view/view.component';
 

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Employees'
      },
      children: [
        {
            path: '',
            component: EmployeeComponent,
            // data: {
            //   title: 'Employees'
            // }
          },
          {
            path:':id',
            component:ViewComponent,
            data:{
              tittle:'Animals'
            }
          }
     ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class EmployeeRoutingModule {}
  