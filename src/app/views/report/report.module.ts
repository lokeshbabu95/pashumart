import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {ReportComponent} from './report.component';
import {ReportRoutingModule} from './report-routing.module';
import {StateComponent} from './state/state.component';
import {DistrictComponent} from './district/district.component';

import { TableModule } from 'primeng/table';
@NgModule({
    imports: [
      CommonModule,
      ReportRoutingModule,
      TableModule,
      FormsModule
      
    ],
    declarations: [
        ReportComponent,
        StateComponent,
        DistrictComponent
    ]
  })
  export class ReportModule { }