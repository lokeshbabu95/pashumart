import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportComponent} from './report.component';
import {StateComponent} from './state/state.component';
import {DistrictComponent} from './district/district.component';

const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Reports'
      },
      children: [
        {
            path: 'breed',
            component: ReportComponent,
            data: {
              title: 'Reports by Breed'
            }
          },
          {
            path: 'state',
            component: StateComponent,
            data: {
              title: 'Reports by State'
            }
          },
          {
            path: 'district',
            component: DistrictComponent,
            data: {
              title: 'Reports by District'
            }
          },
     ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ReportRoutingModule {}
  