import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
import { ExcelService} from '../../../excel.service';

@Component({
    selector: 'app-animal',
    templateUrl: './state.component.html',
    styleUrls: ['./state.component.scss']
  })
  export class StateComponent implements OnInit {
    dropdownoption = [{name:"Please Select",value:"Please Select"},{name:"Andhra Pradesh",value:"Andhra Pradesh"},{name:"karnataka",value:"karnataka"},{name:"Maharashtra",value:"Maharashtra"},{name:"Telangana",value:"Telangana"}];
    dropdownselected = "Please Select";

    reportdata:any={
     
      state:"",
      start_date:"",
      end_date:""
    };
    tableshow = false;
    reporttabledata = [];
    totalamount;
    cols = [
      {header:"Animal Id",field:"animal_id"},
      { header: "Species", field: "species" },
      { header: "Breed", field: "breed" },
      { header: "Age", field: "animal_age" },
      {header:"State",field:"state"},
      {header:"District",field:"district"},
      { header: "Cost", field: "approx_cost" },
       { header: "Sold Date", field: "sold_date" },
       
      // { header: "Age", field: "animal_age" },
      // { header: "Location", field: "location" },
      // { header: "Avaliability", field: "avaliability" },
      // {header:"Treatment",field:"treatment"}
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router,private excelService:ExcelService) {
   
      //console.log(this.urldata.split("/"));

    }
    ngOnInit() {
    }
    editreport(event){
      if(this.reportdata.state || this.reportdata.start_date || this.reportdata.end_date ){
        this.reporttabledata = [];
        this.apiService.viewreportbystate(this.reportdata).subscribe((data:any)=>{
          this.reporttabledata = data.data;
          this.totalamount = data.total_cost;
          if(this.reporttabledata){
            this.tableshow = true;
          }else{
            console.log("iam here");
          }
    
        })
      }
      else{
        alert("please select all the fields");
      }
     
    }
    exportAsXLSX():void {
      this.excelService.exportAsExcelFile(this.reporttabledata, this.reportdata.state+'State report');
    }
}