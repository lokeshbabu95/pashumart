import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'app-view',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
  })
  export class EditComponent implements OnInit {
    Memberdata :any;
    memberid;
    // health:any=[{
     
    //   treated_on:"",
    //   vaccine:"",
    //   disease:""
    // }];
    constructor(private apiService: ApiService, private route: Router, private domSanitizer: DomSanitizer) {
    }
    ngOnInit() {
      this.memberid = this.route.url.split("/");
      this.apiService.getmemberdetails(this.memberid[3])
      .subscribe((data:any)=>{
        this.Memberdata = data.data[0];
        console.log(this.Memberdata);
      });
        
    }
    editdata(event){
      this.apiService.editmemberdetails(this.Memberdata).subscribe((data:any)=>{
        this.Memberdata = data.data[0];
        console.log(this.Memberdata);
      });
    }
    
    // editUser(event){
    //   console.log(event);
    //   console.log(this.user); 
    //   this.apiService.editanimal(this.user).subscribe((data:any)=>{
    //     console.log(data);
    //   })
    // }
    // edithealth(event){
    //   console.log(this.health);
    //   this.health.forEach((datas,element) => {
    //     console.log(datas,"=============",element,);
    //     this.apiService.edithealth(datas).subscribe((data:any)=>{
    //     console.log(data);
    //   })
    //   });
      
    // }
  }