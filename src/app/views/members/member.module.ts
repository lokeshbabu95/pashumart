import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {MemberComponent} from './member.component';
import {MemberRoutingModule} from './member-routing.module';
import {ViewComponent} from './view/view.component';
import { TableModule } from 'primeng/table';
import{ EditComponent} from './edit/edit.component';
@NgModule({
    imports: [
      CommonModule,
      MemberRoutingModule,
      TableModule,
      FormsModule
    ],
    declarations: [
        MemberComponent,
        ViewComponent,
        EditComponent
    ]
  })
  export class MemberModule { }