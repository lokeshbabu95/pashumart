import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberComponent} from './member.component';
import {ViewComponent} from './view/view.component';
import{ EditComponent} from './edit/edit.component';


const routes: Routes = [
    {
      path: '',
      
      data: {
        title: 'Registered Members'

      },
      children: [
         {
          path: '',
          component: MemberComponent,
          data: {
            title: 'create'
          }
        },
        {
          path:':id',
          component:ViewComponent,
          data:{
            tittle:'view'
          }
        },
        {
          path:'edit/:id',
          component:EditComponent,
          data:{
            tittle:'Edit'
          }
        }
       
     ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class MemberRoutingModule {}
  