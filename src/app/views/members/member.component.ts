import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../api.service';

@Component({
    selector: 'app-member',
    templateUrl: './member.component.html',
    styleUrls: ['./member.component.scss']
  })
  export class MemberComponent implements OnInit {
    Totalmembers = [];
    Totalmemberstemp = [];
    cols = [
      { header: "User Name", field: "mobile_number" },
      { header: "Name", field: "name" },
      { header: "Mobile", field: "mobile_number" },
      { header: "User Type", field: "user_type" },
      {header:"Total Animals",field:"count"},
      {header:"Sold", field:"sale"}
      //{header:""}
    ];
    constructor(private apiService: ApiService, private route: Router) {
    }
    ngOnInit() {
      this.apiService.viewUser()
      .subscribe(
        (data: any) => {
          this.Totalmembers = data.data;
          this.Totalmemberstemp = data.data;
          console.log(data);
        }, error => {
         
        });
   
  }
  viewdata(rowData){
    console.log(rowData);
    this.route.navigate(['/registeredmembers/',rowData.loginid]);
  }
  editMe(rowData){
    console.log(rowData);
    this.route.navigate(['/registeredmembers/edit',rowData.loginid]);
  }

  search(event) {
    let value ;
    if(event.target){
     value = event.target.value.toLowerCase();
    }else{
      value = event.toLowerCase();
    }
    //let value = event.target.value.toLowerCase();
    let temp = [];
    for (let member of this.Totalmembers) {
      for (let key in member) {
        if(member[key] == null){
        }
        else{
          if (member[key].toString().toLowerCase().includes(value)) {
          temp.push(member);
          break;
        }}
        
      }
    }
    this.Totalmembers = temp;
    if (value.length === 0) {
      console.log("iamhere");
      this.Totalmembers = this.Totalmemberstemp;
    }
  }
}