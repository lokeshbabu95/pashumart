import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {AnimalComponent} from './animal.component';
import {AnimalRoutingModule} from './animal-routing.module';
import {ViewComponent} from './view/view.component';
import {EditComponent} from './edit/edit.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ImageCropperModule } from 'ngx-image-cropper';

import { TableModule } from 'primeng/table';
@NgModule({
    imports: [
      CommonModule,
      AnimalRoutingModule,
      TableModule,
      TabsModule,
      ImageCropperModule,
      FormsModule,
      
    ],
    declarations: [
        AnimalComponent,
        ViewComponent,
        EditComponent
    ]
  })
  export class AnimalModule { }