import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimalComponent} from './animal.component';
import {ViewComponent} from './view/view.component';
import {EditComponent} from './edit/edit.component';


const routes: Routes = [
    {
      path: '',
      data: {
        title: 'Animals'
      },
      children: [
        {
          path: '',
          component: AnimalComponent,
          // data: {
          //   title: 'Registered Animals'
          // }
        },
         {
          path: 'animal/:id',
          component: ViewComponent,
          data: {
            title: 'Details'
          }
        },
        {
          path:'animals/:string',
          component:AnimalComponent
        },
        {
          path:'edit/:id',
          component:EditComponent,
          data:{
            title:'Edit'
          }
        }
        
        // {
        //   path: 'form',
        //   component: FormComponent,
        //   data: {
        //     title: 'form'
        //   }
        // }
     ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AnimalRoutingModule {}
  