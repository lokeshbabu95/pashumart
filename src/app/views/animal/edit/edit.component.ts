import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'app-view',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
  })
  export class EditComponent implements OnInit {
    animalId;
    animaldata = [];
    healthdata = [];
    imagedata;
    user: any;
    health:any=[{
     
      treated_on:"",
      vaccine:"",
      disease:""
    }];
    constructor(private apiService: ApiService, private route: Router, private domSanitizer: DomSanitizer) {
    }
    ngOnInit() {
        this.animalId = this.route.url.split("/");
        this.apiService.getanimalbyid(this.animalId[3])
        .subscribe((data:any) =>{
            this.animaldata = data.data[0];
            this.user = data.data[0];
        });
        this.apiService.gethealthbyId(this.animalId[3])
        .subscribe((data:any)=>{
          this.healthdata = data.data;
          this.health = data.data;
        });
        this.apiService.getimage(this.animalId[3])
        .subscribe((data:any)=>{
            this.imagedata = data.data;
        })
    }

    changeListener($event) {
      var file: File = $event.target.files[0];
      var myReader: FileReader = new FileReader();
  
      myReader.onloadend = (e) => {
        if (myReader.result.toString().length <= 5000000) {
          console.log(myReader.result.toString())
        }
        else {
          console.log('too big')
        }
      }
      myReader.readAsDataURL(file);
    }
    editUser(event){
      this.apiService.editanimal(this.user).subscribe((data:any)=>{
        alert("updated sucessfully");
      })
    }
    edithealth(event){
      this.health.forEach((datas,element) => {
        this.apiService.edithealth(datas).subscribe((data:any)=>{
          alert("updated sucessfully");
      })
      });
      
    }
  }