import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../api.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { elementStylingMap } from '@angular/core/src/render3';
import { default as config } from '../../../../config';
@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.scss']
  })
  export class ViewComponent implements OnInit {
    animalId;
    animaldata = [];
    healthdata = [];
    imagedata;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    croppedImage1:any = '';
    url = '';
    API_URL = config.API_URL;
    constructor(private apiService: ApiService, private route: Router, private domSanitizer: DomSanitizer) {
    }
    ngOnInit() {
        this.animalId = this.route.url.split("/");
        this.apiService.getanimalbyid(this.animalId[3])
        .subscribe((data:any) =>{
            this.animaldata = data.data[0];
        });
        this.apiService.gethealthbyId(this.animalId[3])
        .subscribe((data:any)=>{
          this.healthdata = data.data;
        });
        this.apiService.getimage(this.animalId[3])
        .subscribe((data:any)=>{
            this.imagedata = data.data;
            this.imagedata.forEach(element => {
             element.image =  element.image.replace("C:/xampp/htdocs/Pashumart","http://18.191.159.237/api");
            // element.image =  element.image.replace("C:/xampp/htdocs/Pashumart",this.API_URL);
              
            });
        })
    }

    changeListener($event) {
      var file: File = $event.target.files[0];
      var myReader: FileReader = new FileReader();
  
      myReader.onloadend = (e) => {
        if (myReader.result.toString().length <= 5000000) {
        }
        else {
        }
      }
      myReader.readAsDataURL(file);
    }
    

  //   fileChangeEvent(event: any): void {
  //     this.imageChangedEvent = event;
  // }
  // imageCropped(event: ImageCroppedEvent) {
  //   this.croppedImage1 = event.file;
  //     this.croppedImage = event.base64;
  //     console.log(this.croppedImage1);
  //     console.log(this.croppedImage);
  // }
  // imageLoaded() {
  //     // show cropper
  // }
  // cropperReady() {
  //     // cropper ready
  // }
  // loadImageFailed() {
  //     // show message
  // }
  // onSelectFile(event) {
  //   if (event.target.files && event.target.files[0]) {
  //     var reader = new FileReader();

  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onload = (event) => { // called once readAsDataURL is completed
  //       this.url = event.target.result;
  //       console.log(this.url);
  //     }
  //   }
  // }
  }