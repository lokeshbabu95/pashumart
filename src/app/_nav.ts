interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    // badge: {
    //   variant: 'info',
    //   text: ''
    // }
  },
  {
    name:'Partners',
    url:'/partner',
    icon:'fa fa-table',
    children: [
      { 
        name: 'Doctor',
        url: '/partner/doctors',
        // icon: 'fa fa-stethoscope'
      },
      { 
        name: 'Logistics',
        url: '/partner/logistics',
        // icon: 'fa fa-truck'
      },
      { 
        name: 'Bank',
        url: '/partner/loan',
        // icon: 'fa fa-bank'
      },
      { 
        name: 'Food & Nutrition',
        url: '/partner/food',
        // icon: 'fas fa-bread-slice'
      },

     

      ]
  },
  {
    name:'Members',
    url:'/registeredmembers',
    icon:'fa fa-table'
  },
  {
    name:'Employee',
    url:'/employee',
    icon:'fa fa-file-text-o',
    
  },
  {
    name:'Animal ',
    url:'/animalregistration',
    icon:'fa fa-table'
  },
  {
    name:'Settings',
    url:'/setting',
    icon:'fa fa-file-text-o',
    
  },
  
  {
    name:'Reports',
    url:'/report',
    icon:'fa fa-bar-chart',
    children: [
      { 
        name: 'By Breed',
        url: '/report/breed',
        icon: ''
      },
      { 
        name: 'By State',
        url: '/report/state',
        icon: ''
      },
      { 
        name: 'By District',
        url: '/report/district',
        icon: ''
      }

     

      ]
  },
  {
    name:'Request',
    url:'/request',
    icon:'fa fa-table',
    children: [
      { 
        name: 'Add',
        url: '/request/add',
        icon: ''
      },
      { 
        name: 'View',
        url: '/request/view',
        icon: ''
      },
      // { 
      //   name: 'Logistics',
      //   url: '/partner/logistics',
      //   icon: ''
      // },
      // { 
      //   name: 'Loan',
      //   url: '/partner/loan',
      //   icon: ''
      // },
      // { 
      //   name: 'Food & Nutrition',
      //   url: '/partner/food',
      //   icon: ''
      // },

     

       ]
  },
  
];
