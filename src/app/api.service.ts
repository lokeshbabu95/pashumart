import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { default as config } from './../config';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_URL = config.API_URL;
  animalname:string;
  constructor(private httpClient: HttpClient){ 
}
viewUser() {
  //let token = this.localStorageService.get("token");
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/member/all`, { headers: headers });
}
viewAnimals(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/animals/all`, { headers: headers });

}
viewDoctors(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/doctors`, { headers: headers });
}
viewLogistics(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/logistics`, { headers: headers });
}
viewLoan(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/loan`, { headers: headers });
}
viewfood(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/food`, { headers: headers });
}
viewemployee(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/employee`, { headers: headers });
}
// getdashboard(){
//   let headers = new HttpHeaders();
//   return this.httpClient.get(`${this.API_URL}/dashboard`, { headers: headers });
// }
getdashboardbyanimal(id){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/dashboard/animal/`+id, { headers: headers });
}
getdashboardbystate(id){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/dashboard/state/`+id, { headers: headers });
}
getdashboardbydistrict(name,id){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/dashboard/state/`+name+'/'+id, { headers: headers });
}
getdashboardbydate(id){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/dashboard/date1/` + id, { headers: headers });
}
addrequest(requestdata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/requestdata`, requestdata, { headers: headers });
}
viewrequest(){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/request/all`, { headers: headers });
}
getanimalbyid(animalid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/animal/` + animalid , { headers: headres });
}
gethealthbyId(animalid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/animal/health/` + animalid , { headers: headres });
}
getimage(animalid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/animal/image/` + animalid , { headers: headres });
}
viewdoctorbyId(doctorid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/partner/doctor/` + doctorid , { headers: headres });
}
viewlogisticsbyId(logisticsid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/partner/logistics/` + logisticsid , { headers: headres });
}
getmemberbyid(memberid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/member/` + memberid , { headers: headres });
}
viewloanbyId(loginid,loanid){
  let headres = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/partner/loan/`+loginid+'/'+loanid , { headers: headres });
}
editanimal(animaldata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/animal/update/`+ animaldata.animal_id,animaldata,{headers:headers});
}
edithealth(animaldata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/animal/update/health/`+ animaldata.animal_id,animaldata,{headers:headers});
}
getmemberdetails(memberid){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/member/details/`+ memberid,{headers:headers});
}
editmemberdetails(memberdata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/member/edit/`+ memberdata.id,memberdata,{headers:headers});
}

employeemember(emloyeeid){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/employee/member/`+ emloyeeid,{headers:headers});
}
viewfoodbyid(id,foodid){
  let headers = new HttpHeaders();
  return this.httpClient.get(`${this.API_URL}/partner/food/`+id+'/'+foodid,{headers:headers});
}
viewreportbybreed(breeddata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/reportby/breed/`,breeddata,{headers:headers});
}
viewreportbystate(statedata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/reportby/state/`,statedata,{headers:headers});
}
viewreportbydistrict(districtdata){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/reportby/district/`,districtdata,{headers:headers});
}
viewdistrict(statename){
  let headers = new HttpHeaders();
  return this.httpClient.post(`${this.API_URL}/districts/`,statename,{headers:headers});
}

}

